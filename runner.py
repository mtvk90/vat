import multiprocessing
from datasets import load_metric
import torchaudio
from transformers import Trainer, TrainingArguments
from transformers import Wav2Vec2ForCTC, Wav2Vec2Processor
from dataloader.data_collator import DataCollatorCTCWithPadding
import torch
import numpy as np
from dataloader.dataloder import DataLoader
import librosa
from utils.runtime_config import RuntimeConfig
class Runner:
    def __init__(self) -> None:
        self.base_model_name = RuntimeConfig.BASE_MODEL_NAME
        self.output_dir = RuntimeConfig.CHECKPOINT_DIR
        self.device = torch.device("cuda" if torch.cuda.is_available() else "cpu")
        self.model = self._get_model()
        self.processor = self._get_processor()
        self.data_collator = self._get_data_collator()
        
        dl = DataLoader()
        self.train_ds = dl.train_ds
        self.test_ds = dl.test_ds

    def _get_model(self):
        return Wav2Vec2ForCTC.from_pretrained(self.base_model_name).to(self.device)

    def _get_processor(self):
        return Wav2Vec2Processor.from_pretrained(self.base_model_name)
    
    def _get_data_collator(self):
        return DataCollatorCTCWithPadding(processor=self.processor, padding=True)

    def _compute_metrics(self, pred):
        wer_metric = load_metric("wer")

        pred_logits = pred.predictions
        pred_ids = np.argmax(pred_logits, axis=-1)

        pred.label_ids[pred.label_ids == -100] = self.processor.tokenizer.pad_token_id

        pred_str = self.processor.batch_decode(pred_ids)
        # we do not want to group tokens when computing the metrics
        label_str = self.processor.batch_decode(pred.label_ids, group_tokens=False)

        wer = wer_metric.compute(predictions=pred_str, references=label_str)

        return {"wer": wer}

    def _prepare_dataset_fn(self, batch):
        speech_array, sampling_rate = torchaudio.load(batch["path"])
        speech_array = speech_array.mean(0).squeeze().numpy()
        speech_array = librosa.resample(np.asarray(speech_array), orig_sr=sampling_rate, target_sr=self.processor.feature_extractor.sampling_rate)

        # batched output is "un-batched" to ensure mapping is correct
        batch["input_values"] = self.processor(speech_array, sampling_rate=self.processor.feature_extractor.sampling_rate).input_values[0]
        batch["speech"] = speech_array
        
        with self.processor.as_target_processor():
            batch["labels"] = self.processor(batch["sentence"]).input_ids
        return batch
    
    def _prepare_datasets(self):
        self.test_ds = self.test_ds.map(self._prepare_dataset_fn, num_proc=1)
        self.train_ds = self.train_ds.map(self._prepare_dataset_fn, num_proc=1)
    
    def _get_trainer_args(self):
        training_args = TrainingArguments(
            output_dir=self.output_dir,
            group_by_length=True,
            per_device_train_batch_size=8,
            evaluation_strategy="steps",
            num_train_epochs=10,
            # fp16=True,
            gradient_checkpointing=True,
            save_steps=500,
            eval_steps=500,
            logging_steps=500,
            learning_rate=1e-4,
            weight_decay=0.005,
            warmup_steps=1000,
            save_total_limit=2,
        )

        return training_args
    
    def _get_trainer(self):
        trainer = Trainer(
            model=self.model,
            data_collator=self.data_collator,
            args=self._get_trainer_args(),
            compute_metrics=self._compute_metrics,
            train_dataset=self.train_ds,
            eval_dataset=self.test_ds,
            tokenizer=self.processor.feature_extractor,
        )

        return trainer

    def run(self):
        self._prepare_datasets()
        
        self.model.freeze_feature_encoder()
        trainer = self._get_trainer()
        trainer.train()

        self.model.save_pretrained(RuntimeConfig.MODEL_SAVE_PATH)
        self.processor.save_pretrained(RuntimeConfig.PROCESSOR_SAVE_PATH)
