from fileinput import filename
from utils.normalizer import normalizer
import pandas as pd
import glob
import os
from sklearn.model_selection import train_test_split
from parsivar import Normalizer
import re
from datasets import Dataset
import numpy as np
# import torchaudio
# import librosa
from pyunpack import Archive
import shutil
import wget 
import tarfile
import sys
from utils.runtime_config import RuntimeConfig

from pandarallel import pandarallel
pandarallel.initialize(progress_bar=False)


class DataLoader():
    def __init__(self) -> None:
        self.train_df = None
        self.test_df = None

        self.common_voice_data_dir = RuntimeConfig.COMMON_VOICE_DATA_DIR
        self.collected_voice_data_dir = RuntimeConfig.COLLECTED_VOICE_DATA_DIR

        self.common_voice_link = RuntimeConfig.COMMON_VOICE_LINK
        self.collected_voice_drive_path = RuntimeConfig.COLLECTED_VOICE_DRIVE_PATH

        self.env = RuntimeConfig.ENV
    
    @staticmethod
    def _bar_progress(current, total, width=80):
        progress_message = "Downloading: %d%% [%d / %d] bytes" % (current / total * 100, current, total)
        # Don't use print() as it will print in new line every time.
        sys.stdout.write("\r" + progress_message)
        sys.stdout.flush()

    def _download_common_voice_dataset(self):
        print('downloading common voice dataset')
        file_name = os.path.split(self.common_voice_link)[1]
        if not os.path.exists(file_name):
            wget.download(self.common_voice_link, out='.')
        tarfile.open(file_name).extractall('.')

    def _download_collected_voice_dataset(self):
        print('downloading collected voice dataset')
        if self.env == 'colab':
            shutil.copy(self.collected_voice_drive_path, '.')
            file_name = os.path.split(self.collected_voice_drive_path)[1]
        else:
            return

        if not os.path.exists(self.collected_voice_data_dir): os.makedirs(self.collected_voice_data_dir)
        Archive(file_name).extractall(self.collected_voice_data_dir)

    @staticmethod
    def _cleaning(text):
        chars_to_ignore_regex = '[\,\?\.\!\-\;\:\"\=]'
        if not isinstance(text.sentence, str):
            return None

        text.sentence = re.sub(chars_to_ignore_regex, '', text.sentence).lower()
        return normalizer({"sentence": text.sentence}, return_dict=False)

    @staticmethod
    def _is_useful(text):
        numbers = ["یک", "دو", "سه", "چهار", "پنج", "شش", "هفت", "هشت", "نه", "ده", "یازده", "دوازده", "سیزده", "چهارده", "پانزده", "شانزده", "هفده", "هجده", "نوزده", "بیست", "سی", "چهل", "پنجاه", "شصت", "هفتاد", "هشتاد", "نود", "صد", "دویست", "سیصد", "چهارصد", "پانصد", "ششصد", "هفتصد", "هشتصد", "نهصد", "هزار", "میلیون", "میلیارد"]
        needed_words = ['خرید', 'فروش', 'ریال', 'تومان', 'بخر' ,'بفروش', 'نماد', 'سهم']
        
        text_split = text.split()
        for num in numbers + needed_words:
            if num in text_split:
                return True
        return False

    def _process_common_voice_dataset(self):
        print('processing common voice dataset')
        if not os.path.exists(self.common_voice_data_dir):
            self._download_common_voice_dataset()

        for file in glob.glob(self.common_voice_data_dir+'/*.tsv'):
            pd.read_csv(file, sep='\t').to_csv(file.replace('.tsv', '.csv'), index=False)
        
        cv_train = pd.read_csv(self.common_voice_data_dir + '/train.csv')

        cv_train['sentence'] = cv_train.parallel_apply(self._cleaning, axis=1)
        cv_train.path = cv_train.path.apply(lambda x: os.path.join(self.common_voice_data_dir, 'clips', x))

        cv_train = cv_train[cv_train.sentence.apply(self._is_useful)][['path', 'sentence']]

        return cv_train
        
    def _process_collected_voice_dataset(self):
        print('processing collected voice dataset')
        if not os.path.exists(self.collected_voice_data_dir):
            self._download_collected_voice_dataset()
        
        parsivar_normalizer = Normalizer()
        collected_voice_files = glob.glob(self.collected_voice_data_dir + '/*')

        collected_voice_df = pd.DataFrame(collected_voice_files, columns=['path'])
        collected_voice_df['sentence'] = collected_voice_df.path.apply(lambda path: parsivar_normalizer.normalize(os.path.split(path)[1].split('.')[0].split('_')[1]))

        return collected_voice_df
    
    def _train_test_split(self, test_size=0.1):
        collected_voice_dataset = self._process_collected_voice_dataset()
        common_voice_dataset = self._process_common_voice_dataset()

        df = pd.concat([collected_voice_dataset, common_voice_dataset])
        self.train_df, self.test_df = train_test_split(df, test_size=test_size)

        return self.train_df, self.test_df

    @property
    def train_ds(self):
        if self.train_df is None:
            self._train_test_split()
        
        return Dataset.from_pandas(self.train_df)
    
    @property
    def test_ds(self):
        if self.test_df is None:
            self._train_test_split()
        
        return Dataset.from_pandas(self.test_df)
        