
class RuntimeConfig:
    BASE_MODEL_NAME: str = 'm3hrdadfi/wav2vec2-large-xlsr-persian-v3'
    CHECKPOINT_DIR: str = 'drive/MyDrive/speech/check'
    MODEL_SAVE_PATH: str = 'speech/model'
    PROCESSOR_SAVE_PATH: str = 'speech/processor'
    COMMON_VOICE_DATA_DIR: str = './cv-corpus-9.0-2022-04-27/fa'
    COLLECTED_VOICE_DATA_DIR: str = './ds16k'
    COMMON_VOICE_LINK: str = 'https://mozilla-common-voice-datasets.s3.dualstack.us-west-2.amazonaws.com/cv-corpus-9.0-2022-04-27/cv-corpus-9.0-2022-04-27-fa.tar.gz'
    COLLECTED_VOICE_DRIVE_PATH: str = "drive/MyDrive/speech/ds16k.7z"
    ENV: str = 'colab'